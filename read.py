import serial
import webbrowser, sys

args_int = sys.argv[1]
port = 'COM' + args_int
baud = 9600
ser = serial.Serial(port, baud, timeout=0)

blue_card = "blue_card.JPG"
white_card = "white_card.JPG"
chrome_path = 'C:/Program Files/Google/Chrome/Application/chrome.exe %s'

while True:
    try:
        card_id = 0
        for line in ser.read():
            card_id = int(ser.readline())
        if card_id:
            if card_id == 46:
                webbrowser.get(chrome_path).open(blue_card)
            if card_id == 80:
                webbrowser.get(chrome_path).open(white_card)      
    except KeyboardInterrupt:
        break

sys.exit(1)
